"""
mailosaur_helper
A wrapper around the mailosaur API
"""
import json
import logging

from qaweber.mh_email import Email as Email
from qaweber.http_base import BaseHTTP

DEFAULT_MAILBOX = "********"
BASE_URL = "https://mailosaur.com/api"


class MailosaurHelper(BaseHTTP):
    """
    MailosaurHelper
    Class that defines the wrapper methods around the mailosaur api
    """
    def __init__(self, api_key, mailbox_key=None):
        """
        Instantiate the MailosaurHelper class
        :param api_key: Mailosaur API key
        :param mailbox_key: Mailosaur mailbox to use (default: se1a93xt)
        """
        self.api_key = api_key
        self.params = {"key": self.api_key}
        self.mailbox_key = mailbox_key if mailbox_key else DEFAULT_MAILBOX
        self.last_email = {}
        self.last_emails = []
        self.log = logging.getLogger(__name__)

    def set_mailosaur_account(self, api_key, mailbox_key):
        """
        Set new account/mailbox to use
        :param api_key: Mailosaur API key
        :param mailbox_key: Mailosaur mailbox to use (default: se1a93xt)
        """
        self.api_key = api_key
        self.mailbox_key = mailbox_key
        self.params = {"key": self.api_key}

    def get_emails(self, items_per_page=50, retries=3, **kwargs):
        """
        Get all emails sent to the set Mailosaur mailbox
        :param items_per_page: How many emails to return in set
        :param **kwargs: Filter criteria for search
        :return emails: List of Email objects or empty list
        """
        req_params = self.params.copy()
        req_params["itemsPerPage"] = items_per_page
        if kwargs:
            req_params.update(kwargs)
        url = "/".join([BASE_URL, "mailboxes", self.mailbox_key, "emails"])
        resp = self.http_get(url, params=req_params, retries=3)
        self.log.debug("Status code received from mailosaur %d",
                       resp.get("status"))
        if resp.get("status") != 200:
            self.log.error("Status received from mailosaur: %d",
                           resp.get("status"))
            return []
        return [Email(k) for k in resp.get("response")]

    def get_emails_to(self, to_address):
        """
        Wrapper around get_emails that passes in an address to search for
        :param to_address: Email address to search for
        :return emails[0].subject: First Subject returned in emails list
        """
        emails = self.get_emails(recipient=to_address)
        return emails

    def search_for_email(self, retries=3, **kwargs):
        """
        Takes a dictionary of search criteria and searches the mailbox
        for an email that matches the criteria.
        Sets self.last_email to the first entry in the search results
        :param kwargs: criteria to search on
        :return True/False: Returns False if no emails found, otherwise True
        """
        email_list = []
        self.log.info(kwargs.get("sub_contains"))
        if "recipient" in kwargs:
            email_list = self.get_emails_to(kwargs.get("recipient"))
        else:
            email_list = self.get_emails()
        # Filter by subject if subject is present in kwargs
        if "sub_contains" in kwargs:
            email_list = [
                email for email in email_list
                if email.subject.find(kwargs.get("sub_contains")) > -1
            ]
        if "subject" in kwargs:
            email_list = [
                email for email in email_list
                if email.subject == kwargs.get("subject")
            ]
        if "sender" in kwargs:
            email_list = [
                email for email in email_list
                if email.get("from")[0].address == kwargs.get("sender")
            ]
        if "text" in kwargs:
            email_list = [
                email for email in email_list
                if email.to[0].address == kwargs.get("text")
            ]
        if not email_list:
            return False
        self.last_email = email_list[0]
        self.last_emails = email_list
        return True

    def get_raw_email(self, ms_email_id, retries=3):
        """
        Retrieves an email from mailosaur in eml RFC822 format
        :param str ms_email_id: The mailosaur email uuid
        :returns str: The RFC822 ascii representation of the email
        """
        url = "/".join([BASE_URL, "files", "email", ms_email_id])
        resp = self.http_get(url, auth=(self.api_key, ""))
        return resp["response"]

    def get_email_body_text(self, email=None):
        """
        Returns the body text of the email
        :param email: Email object to have the text parsed
            from (self.last_email if None)
        :return email.text.body: Body of the email object
        """
        email = email if email else self.last_email
        return email.text.body

    def get_email_body_html(self, email=None):
        """
        Returns the html body of the email
        :param email:
        :return str email.html.body:  HTML body of email object
        """
        email = email if email else self.last_email
        return email.html.body

    def delete_last_email(self, email=None):
        """
        Deletes the email with the specified id from the mailosaur test mailbox
        :param email: Email object to have the text parsed from
            (self.last_email if None)
        """
        email = email if email else self.last_email
        url = "/".join([BASE_URL, "emails", email.id])
        resp = self.http_delete(url, params=self.params)
        return resp.get("response")

    def create_mailbox(self, mailbox_name):
        """
        Creates a new mailbox
        :param mailbox_name: What the readable name of the mailbox should be
        :return mailbox_id: mailbox ID of the new box (False if no box created)
        """
        headers = {"content-type": "application/json"}
        payload = {"name": mailbox_name}
        url = "/".join([BASE_URL, "mailboxes"])
        post_resp = self.http_post(url,
                                   params=self.params,
                                   headers=headers,
                                   data=json.dumps(payload))
        resp = post_resp.get("response")
        try:
            mailbox_id = resp["id"]
            self.log.info("Mailbox ID is: %s", mailbox_id)
            return mailbox_id
        except ValueError:
            self.log.warning(
                "No mailbox was created. Please check the API key.")
        except AttributeError:
            self.log.warning("Mailbox not created. %s", resp)
        return False

    def delete_mailbox(self, mailbox_id=None):
        """
        Deletes the mailbox with the specified mailbox_id. WILL NOT DELETE the
            default mailbox
        :param mailbox_id: ID of the mailbox to delete
            (defaults to self.mailbox_id)
        """
        if not mailbox_id:
            mailbox_id = self.mailbox_key
        if mailbox_id == DEFAULT_MAILBOX:
            self.log.warning("The current mailbox is set to the default box. \
                    This will not be deleted")
            return
        url = "/".join([BASE_URL, "mailboxes", mailbox_id])
        self.http_delete(url, params=self.params)
        self.log.info(
            "Delete request finished. Please verify that mailbox with ID "
            "%s has been deleted.",
            mailbox_id,
        )

    def get_all_links_from_email(self, email=None):
        """
        Gets all links from the specified email
        :param email: Email object to have the text parsed from
            (self.last_email if None)
        :return links: List of links objects
        """
        email = email if email else self.last_email
        link_dict = (email.html.links
                     if email.html.links is not None else email.text.links)
        links = [link_section for link_section in link_dict]
        return links

    def click_link_in_email(self, target_link, email=None):
        """
        Clicks the target link in the specified email
        :param target_link: an identifiable part of the link to be clicked
            (i.e. - google.com)
        :param email: Email object to have the text parsed from
            (self.last_email if None)
        :return True/False: Returns True if status == 200, otherwise False
        """
        email = email if email else self.last_email
        links = self.get_all_links_from_email(email=email)
        link_url = [link.href for link in links if target_link in link.text]
        if not link_url:
            return False
        resp = self.http_get(url=link_url[0])
        return resp["status"] == 200

    def click_link_and_return_redirect_history(self, target_link, email=None):
        """
        Click a link in an email and return the response history as a list
        of status codes.
        If the link is found in the email, returns a dict of this form:
                {'http_code_history': [<int>, <int>...],  List of status codes
                  'url_history': [<str>, <str>...],  List of url's hit
                  'url_final': <str>,  Final URL after all redirects
                  'code_final': <str>  Final http status code after redirects
                  }
        :param str target_link: plaintext of link in email
        :param dict email: email json record from mailosaur
        :return dict: Dict containing response info status codes or
                False if link not found
        """
        email = email if email else self.last_email
        links = self.get_all_links_from_email(email=email)
        link_url = [link.href for link in links if target_link in link.text]
        if not link_url:
            return False
        resp = self.http_get(url=link_url[0], allow_redirects=True, retries=5)
        self.log.debug("response is %r", resp["history"])
        self.log.debug(
            "response_url_history is %r",
            [int_response.url for int_response in resp["history"]],
        )
        self.log.debug("response lands on url: %s", resp["url"])
        self.log.debug("response code is %d", resp["status"])
        self.log.debug("raw email href is %s", link_url[0])
        result = {
            "http_code_history":
            [code.status_code for code in resp["history"]],
            "url_history":
            [int_response.url for int_response in resp["history"]],
            "url_final": resp["url"],
            "code_final": resp["status"],
            "raw_email_url": link_url[0],
        }
        return result

    def click_tracking_pixel(self, email=None):
        """
        Simulates an open of an AWeber email by performing a get
            on the tracking pixel URL
        :param email: Email object to have the text parsed from
            (self.last_email if None)
        :return True/False: Returns True if status == 200, otherwise False
        """
        email = email if email else self.last_email
        image_url = [
            image.src for image in email.html.images if "openrate" in image.src
        ]
        if not image_url:
            return False
        resp = self.http_get(url=image_url[0])
        return resp["status"] == 200

    def get_email_headers(self, email=None):
        """
        Gets the headers from the specified email
        :param email: Email object to have the text parsed from
            (self.last_email if None)
        :return email.headers: The headers within the email
        """
        email = email if email else self.last_email
        return email.headers

    def get_header_value(self, header_to_find, headers=None):
        """
        Gets the value of a specific header
        :param header_to_find: The header to look for in an email
        :param headers: The list of headers to look in (retrieves headers from
            email if not entered)
        """
        if not headers:
            headers = self.get_email_headers()
        return headers.get(header_to_find, "No Header Found")

    def empty_mailbox(self, mailbox_id=None):
        """Empties a mailbox
        :param str mailbox_id: ID of the mailbox to empty
        :return bool: Returns True if status = 204 else False
        """
        if not mailbox_id:
            raise AttributeError("No mailbox_id specified")
        url = "/".join([BASE_URL, "messages"])
        params = {"server": mailbox_id}
        resp = self.http_delete(url, params=params, auth=(self.api_key, ""))
        return resp["status"] == 204

    def get_attachment(self, attachment_id):
        """ Retrieves an attachment from mailosaur by ID
        :param str attachment_id: UUID of attachment in mailoasur
        """
        url = "/".join([BASE_URL, "attachments", attachment_id])
        resp = self.http_get(url, auth=(self.api_key, ""))
        return resp
    
