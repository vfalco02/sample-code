""" HTTP_Base
package that contains base methods for interacting with APIs
"""
import os
import geturl
import logging
import requests
from requests import HTTPError
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from simplejson import JSONDecodeError

requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True
HTTP_TIMEOUT = int(os.environ.get("HTTP_TIMEOUT", "5"))


def retry_session(retries,
                  backoff_factor=0.5,
                  status_forcelist=(500, 502, 504, 429),
                  session=None):
    """
    Return request session that is retried
    Retries the passed in session multiple times using the
    requests.packages.urllib3.util.retry.Retry class.
    By default this class retries only idempotent http methods
    ['HEAD', 'TRACE', 'GET', 'PUT', 'OPTIONS', 'DELETE']
    It does not retry post requests.  That can be implemented later,
    but may have undesirable side effects.  If you want to retry
    a post request, do so at a higher level than this.
    :param retries int: Number of retries to attempt (total).  Defaults
      to 1 to mimic existing behavior
    :param backoff_factor int: incremental backoff time per request
    :param status_forcelist tuple: tuple of http response codes to
       retry
    :param session: Requests.session object passed in
    :return: requests.Session object
    """
    session = session or requests.Session()
    retry = Retry(total=retries,
                  backoff_factor=backoff_factor,
                  status_forcelist=status_forcelist)
    adapter = HTTPAdapter(max_retries=retry)
    session.mount("http://", adapter)
    session.mount("https://", adapter)
    return session


class BaseHTTP:
    """
    BaseHTTP
    Methods for interacting with APIs
    """
    @staticmethod
    def _http(method, url, retries=0, **kwargs):
        """
        Private method utilized by all http_ methods to send and log HTTP
            requests.
        :param method: GET, POST, PUT, PATCH, DELETE
        :param url: the URL to perform the request on
        :param retries: Number of times to retry the request.
          default to 0..
           Only works for idempotent requests
        :param kwargs: any additional params to pass into the request
        :envvar HTTP_TIMEOUT: default timeout for http requests.
          defaults to 5 seconds.  Overriden by timeout passed into
          this method in kwargs
        """
        result = {"status": None, "response": None, "http_error": None}
        if "timeout" not in kwargs:
            kwargs["timeout"] = HTTP_TIMEOUT
        session = requests.session()
        response = retry_session(session=session, retries=retries).request(
            method, url, **kwargs)
        result["status"] = response.status_code
        result["history"] = response.history
        result["headers"] = response.headers
        result["url"] = response.url
        result["content"] = response.content
        try:
            response.raise_for_status()
        except HTTPError as exc:
            result["http_error"] = exc
        try:
            resp = (response.json()
                    if "application/json" in response.headers.get(
                        "content-type", []) else response.text)
        except JSONDecodeError:
            resp = response.text
        result["response"] = resp
        return result

    def http_delete(self, url, retries=0, **kwargs):
        """
        Helper routine to send DELETE http requests
        :param url: the URL to perform the request on
        :param retries: Number of times to retry the request.
          default to 0.
        :param kwargs: any additional params to pass into the request
        :return: response of the request
        """
        return self._http("DELETE", url, retries=retries, **kwargs)

    def http_get(self, url, retries=0, **kwargs):
        """
        Helper routine to send GET http requests
        :param url: the URL to perform the request on
        :param retries: Number of times to retry the request.
          default to 0.
        :param kwargs: any additional params to pass into the request
        :return: response of the request
        """
        return self._http("GET", url, retries=retries, **kwargs)

    def http_post(self, url, retries=0, **kwargs):
        """
        Helper routine to send POST http requests
        :param url: the URL to perform the request on
        :param retries: Number of times to retry the request.
          default to 0.
        :param kwargs: any additional params to pass into the request
        :return: response of the request
        """
        return self._http("POST", url, retries=retries, **kwargs)

    def http_patch(self, url, retries=0, **kwargs):
        """
        Helper routine to send PATCH http requests
        :param url: the URL to perform the request on
        :param retries: Number of times to retry the request.
          default to 0.
        :param kwargs: any additional params to pass into the request
        :return: response of the request
        """
        return self._http("PATCH", url, retries=retries, **kwargs)

    def http_put(self, url, retries=0, **kwargs):
        """
        Helper routine to send PUT http requests
        :param url: the URL to perform the request on
        :param retries: Number of times to retry the request.
          default to 0.
        :param kwargs: any additional params to pass into the request
        :return: response of the request
        """
        return self._http("PUT", url, retries=retries, **kwargs)

    def get_service_url(self, service_name, *args):
        """Gets the service url by using geturl library
        :param str service_name: the name of the service
        :param args: optional list of path segments
        :return str: URL for the requested service
        """
        envs = ["staging", "production", "testing"]
        env = os.environ.get("ENVIRONMENT").lower()
        if env not in envs:
            raise ValueError("ENVIRONMENT var is '{}'. However, \
                it must be set to one of the options {}".format(env, envs))

        consul_services = {
            "services": {
                # List services and consul addresses here.
            }
        }

        aws_services = ["bulk-tagging", "tagging", "mapping"]

        if service_name in aws_services:
            return geturl.get_service_url(service_name, *args)
        if service_name in consul_services["services"]:
            return geturl.get_service_url(service_name,
                                          *args,
                                          settings=consul_services)
        raise ValueError(
            'Provided service name "{}" is not valid '.format(service_name))
