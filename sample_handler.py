class BlockedHandler(BaseHandler):
    async def get(self):
        """Checks if a key exists for the specified email and returns a
        response stating if the email is blocked and when the block expires.
    
        :param str email: The email address to check
        :raises problemdetails.Problem: when no email parameter passed
        """
        email = self.get_query_argument("email", None)
        if email:
            try:
                email_validation.validate_email(email)
            except email_validation.ValidationError as error:
                self.logger.error('invalid email address %r: %s', email, error)
                raise problemdetails.Problem(
                    400,
                    title='Invalid email parameter',
                    detail=f'{email} is not a valid email address')
        else:
            self.logger.error('No email address passed in')
            raise problemdetails.Problem(
                400,
                title='Missing email query parameter',
                detail=f'Did not find email in query parameters')
        key = f'{email}-next'

        self.logger.info('Checking if email: %s is blocked', email)
        expiration = await self.application.throttler.redis.get_expiration(key)

        blocked = False
        block_expiration = None
        if expiration >= 0:
            self.logger.info('Block exists for email: %s with a TTL of %s',
                             email, expiration)
            blocked = True
            block_expiration = arrow.utcnow().shift(
                seconds=expiration).isoformat()
        elif expiration == -2:
            self.logger.info('Block does not exist for email: %s', email)
            blocked = False
        elif expiration == -1:
            blocked = True
            self.logger.error('Key: %s exists with no associated expiration',
                              key)
            if self.sentry_client:
                self.sentry_client.captureMessage(
                    'Key found without associated expiration',
                    params={
                        'email': email,
                        'key': key,
                    })
        else:
            self.logger.error(
                'Unexpected expire value received from redis: %s', expiration)
            if self.sentry_client:
                self.sentry_client.captureMessage(
                    'Unexpected redis expire value received',
                    params={
                        'email': email,
                        'key': key,
                        'expire': expiration
                    })
            raise problemdetails.Problem(
                500,
                title='Unexpected Redis expire value',
                detail=('An unexpected expire value was received from redis: '
                        f'{expiration}'))

        self.send_response({
            'blocked': blocked,
            'block_expiration': block_expiration
        })


class UnblockHandler(BaseHandler):
    async def post(self):
        """Takes an email in the request body and removes the block from redis.
        
        :param str email: The email address to unblock
        :raises problemdetails.Problem: when no/invalid email parameter passed
            or redis throws an unexpected error
        """
        body = self.get_request_body()
        email = body.get('email')
        if email:
            try:
                email_validation.validate_email(email)
            except email_validation.ValidationError as error:
                self.logger.error('invalid email address %r: %s', email, error)
                raise problemdetails.Problem(
                    400,
                    title='Invalid email address',
                    detail=f'{email} is not a valid email address')
        else:
            self.logger.error('No email address in body')
            raise problemdetails.Problem(
                400,
                title='Missing email in request body',
                detail='Did not find email in request body')
        key = f'{email}-next'

        deleted = await self.application.throttler.redis.delete_key(key)
        if deleted >= 0:
            self.set_status(204)

        raise problemdetails.Problem(
            500,
            title='Unexpected Redis delete response',
            detail=(
                'An unexpected delete response was received from redis: '
                f'{deleted}'))
